#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <glib.h>
#include <gtk/gtk.h>
#include <gio/gdesktopappinfo.h>
#include "config.h"

#define ICON_SIZE 72
//#define ICON_UNKNOWN "gnome_unknown"
#define ICON_UNKNOWN "binary"
#define BUTTON_ROW_SPACING 40
#define BUTTON_COLUMN_SPACING 20
#define MAX_BUTTONS_IN_ROW 5

GHashTable *apps = NULL;
GList *visible_apps = NULL;

/**
 * Sets the application style sheet.
 *
 * @return int
 */
int ab_set_css_styles()
{
    GError *error = 0;
    GdkDisplay *display;
    GdkScreen *screen;
    GtkCssProvider *provider;
    struct stat sb;
    int ret = 1; // 1: Not found. 0: Found

    char buf[255];
    snprintf(buf, sizeof(buf), "%s/%s", getenv("HOME"), ".local/share/aboard/styles.css");

    const gchar *css_files[3];
    css_files[0] = "./styles.css";
    css_files[1] = buf;
    css_files[2] = INSTALLATION_PREFIX"/share/aboard/styles.css";

    display = gdk_display_get_default();
    screen = gdk_display_get_default_screen(display);
    provider = gtk_css_provider_new();
    gtk_style_context_add_provider_for_screen(screen, GTK_STYLE_PROVIDER(provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

    for (int i = 0; i < 3; i++) {

        if (stat(css_files[i], &sb) == 0) {

            gtk_css_provider_load_from_file(provider, g_file_new_for_path(css_files[i]), &error);

            if (error != NULL) {

                g_error_free(error);
            } else {

                ret = 0;
                break;
            }
        }
    }

    g_object_unref(provider);

    if (ret == 1) {
        g_warning("[ERROR]: The stylesheet could not be found.");
    }

    return ret;
}

/**
 * Destroy the main window.
 *
 * @param GtkWidget* widget
 * @param gpointer data
 * @return void
 */
void ab_window_destroy(GtkWidget *widget, gpointer data)
{
    gtk_main_quit();
}

/**
 * Launch an application.
 *
 * @param GAppInfo* app_info
 * @return gboolean
 */
gboolean ab_app_launch(GAppInfo *app_info)
{
    GError *error = NULL;

    gboolean success = g_app_info_launch(app_info, NULL, NULL, &error);
    g_assert(success == TRUE || error != NULL);

    if (!success || error != NULL) {

        const gchar* app_name = g_app_info_get_name(app_info);

        g_warning("[ERROR]: Error launching %s => %d: %s", app_name, error->code, error->message);
        g_error_free(error);

        return FALSE;
    }

    ab_window_destroy(NULL, NULL);

    return TRUE;
}

/**
 * Handle the key release event and take actions depending on the pressed keys.
 *
 * @param GtkWidget* widget
 * @param GdkEvent* event
 * @param gpointer user_data
 * @return gboolean
 */
gboolean ab_window_key_release(GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
    GdkEventKey *event_key = (GdkEventKey*) event;

    if (event_key->keyval == GDK_KEY_Escape) {
        ab_window_destroy(NULL, NULL);
    }

    if (event_key->keyval == GDK_KEY_Return) {

        if (g_list_length(visible_apps) == 1) {

            const gchar *key = (gchar*) g_list_first(visible_apps)->data;
            GAppInfo *app_info = (GAppInfo*) g_hash_table_lookup(apps, (gconstpointer) key);

            if (app_info != NULL) {
                ab_app_launch(app_info);
            }
        }
    }

    // Propagate
    return FALSE;
}

/**
 * Responds to the search entry's search-changed event. Invalidates the flow box
 * filter to refresh the showed buttons.
 *
 * @param GtkWidget* widget
 * @param gpointer user_data
 * @return void
 */
void ab_searchentry_changed(GtkWidget *widget, gpointer user_data)
{
    g_list_free(visible_apps);
    visible_apps = NULL;

    gtk_flow_box_invalidate_filter((GtkFlowBox*) user_data);
}

/**
 * Sort buttons contained in the flow box alphabetically (by application name).
 *
 * @param GtkFlowBoxChild* child1
 * @param GtkFlowBoxChild* child2
 * @param gpointer user_data
 * @return gint
 */
gint ab_flowbox_sort_applications(GtkFlowBoxChild *child1, GtkFlowBoxChild *child2, gpointer user_data)
{
    GList *button_container = gtk_container_get_children(GTK_CONTAINER(child1));
    GtkButton *button = (GtkButton*) button_container->data;
    const gchar *label1 = gtk_button_get_label(button);

    button_container = gtk_container_get_children(GTK_CONTAINER(child2));
    button = (GtkButton*) button_container->data;
    const gchar *label2 = gtk_button_get_label(button);

    return g_strcmp0(label1, label2);
}

/**
 * Filter buttons contained in the flow box depending on the introduced search term.
 * The filter is performed by application name (button label) and category.
 *
 * @param GtkFlowBoxChild* child
 * @param gpointer user_data
 * @return gboolean
 */
gboolean ab_flowbox_filter_applications(GtkFlowBoxChild *child, gpointer user_data)
{
    GtkSearchEntry *search = (GtkSearchEntry*) user_data;
    const gchar *search_term = gtk_entry_get_text(GTK_ENTRY(search));

    if (search_term == NULL) {
        return TRUE;
    }

    GList *button_container = gtk_container_get_children(GTK_CONTAINER(child));
    GtkButton *button = (GtkButton*) button_container->data;
    const gchar *label = gtk_button_get_label(button);

    gchar *haystack = g_utf8_strdown(label, -1);
    gchar *needle = g_utf8_strdown(search_term, -1);
    gchar *found = g_strrstr(haystack, needle);

    gboolean name_match = found != NULL;
    gboolean category_match = FALSE;

    GAppInfo *app = (GAppInfo*) g_hash_table_lookup(apps, (gconstpointer) label);

    if (app != NULL) {

        GDesktopAppInfo *desktop_app = (GDesktopAppInfo*) app;

        if (desktop_app != NULL) {

            const char *categories = g_desktop_app_info_get_categories(desktop_app);

            if (categories != NULL) {

                gchar **categories_vector = g_strsplit(categories, ";", -1);
                gchar **iter = categories_vector;

                while (*iter != NULL) {

                    g_free(haystack);

                    haystack = g_utf8_strdown(*iter, -1);
                    found = g_strrstr(haystack, needle);
                    category_match = found != NULL;

                    if (category_match) {
                        break;
                    }

                    iter++;
                }

                g_strfreev(categories_vector);
            }
        }
    }

    g_free(haystack);
    g_free(needle);

    gboolean visible = name_match || category_match;

    if (visible) {
        visible_apps = g_list_prepend(visible_apps, (gpointer) label);
    }

    return visible;
}

/**
 * Button clicked handler. Launches the application.
 *
 * @param GtkWidget* widget
 * @param gpointer user_data
 * @return void
 */
void ab_button_clicked(GtkWidget *widget, gpointer user_data)
{
    GAppInfo *app_info = (GAppInfo*) user_data;

    ab_app_launch(app_info);
}

/**
 * Returns the related app image.
 *
 * @param GAppInfo* app_info
 * @return GtkWidget*
 */
GtkWidget* ab_get_app_image(GAppInfo *app_info)
{
    GtkWidget *app_image = NULL;
    GIcon *icon = g_app_info_get_icon(app_info);
    const gchar *app_name = g_app_info_get_display_name(app_info);

    if (G_IS_THEMED_ICON(icon)) {

        GThemedIcon *themed_icon = G_THEMED_ICON(icon);
        const gchar * const *icon_names = g_themed_icon_get_names(themed_icon);

        app_image = gtk_image_new_from_icon_name(icon_names[0], GTK_ICON_SIZE_DIALOG);

    } else if (G_IS_LOADABLE_ICON(icon)) {

        GLoadableIcon *loadable_icon = G_LOADABLE_ICON(icon);
        GInputStream *icon_input_stream = g_loadable_icon_load(loadable_icon,
            0,
            NULL,
            NULL,
            NULL);

        if (icon_input_stream != NULL) {

            GdkPixbuf *pixbuf = gdk_pixbuf_new_from_stream_at_scale(icon_input_stream,
                ICON_SIZE, ICON_SIZE, TRUE,
                NULL,
                NULL);

            if (pixbuf != NULL) {

                app_image = gtk_image_new_from_pixbuf(pixbuf);
            }
        }

    } else if (G_IS_EMBLEMED_ICON(icon)) {

        g_debug("[DEBUG]: -> Emblemed icon: %s", app_name);

    } else if (G_IS_FILE_ICON(icon)) {

        g_debug("[DEBUG]: -> File icon: %s", app_name);

    } else if (G_IS_BYTES_ICON(icon)) {

        g_debug("[DEBUG]: -> Bytes icon: %s", app_name);

    } else {

        if (icon != NULL) {
            g_debug("[DEBUG]: -> Unknown icon for %s: %s", g_app_info_get_executable(app_info), g_icon_to_string(icon));
        } else {
            g_debug("[DEBUG]: -> Unknown icon for %s", g_app_info_get_executable(app_info));
        }
    }

    if (app_image == NULL) {
        app_image = gtk_image_new_from_icon_name(ICON_UNKNOWN, GTK_ICON_SIZE_DIALOG);
    }

    return app_image;
}

/**
 * Creates and return a button widget.
 *
 * @param GAppInfo* app_info
 * @return GtkWidget*
 */
GtkWidget* ab_button_create(GAppInfo *app_info)
{
    const gchar *app_name = g_app_info_get_display_name(app_info);
    GtkWidget *button = gtk_button_new_with_label(app_name);
    // Relief is the CSS property box-shadow
    // gtk_button_set_relief(GTK_BUTTON(button), GTK_RELIEF_NONE);
    gtk_widget_set_vexpand(button, FALSE);
    gtk_widget_set_valign(button, GTK_ALIGN_START);

    GtkWidget *app_image = ab_get_app_image(app_info);

    if (app_image != NULL) {

        gtk_image_set_pixel_size(GTK_IMAGE(app_image), ICON_SIZE);

        gtk_button_set_image(GTK_BUTTON(button), app_image);
        gtk_button_set_image_position(GTK_BUTTON(button), GTK_POS_TOP);
        gtk_button_set_always_show_image(GTK_BUTTON(button), TRUE);
    }

    g_signal_connect(button, "clicked", G_CALLBACK(ab_button_clicked), (gpointer) app_info);

    return button;
}

/**
 * Initialize the main window.
 *
 * @return int
 */
int ab_window_init()
{

    GtkWidget *window = NULL;
    GtkWidget *scrolled_window = NULL;
    GtkWidget *vbox = NULL;
    GtkWidget *search = NULL;
    GtkWidget *flow_box = NULL;

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), PACKAGE_NAME);

#ifdef DEBUG_ENABLED
    gtk_window_set_keep_above(GTK_WINDOW(window), FALSE);
    gtk_window_set_decorated(GTK_WINDOW(window), FALSE);
    gtk_window_set_resizable(GTK_WINDOW(window), TRUE);
#else
    gtk_window_set_keep_above(GTK_WINDOW(window), TRUE);
    gtk_window_set_decorated(GTK_WINDOW(window), FALSE);
#endif

    gtk_window_maximize(GTK_WINDOW(window));


    vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_box_set_homogeneous(GTK_BOX(vbox), FALSE);
    gtk_container_add(GTK_CONTAINER(window), vbox);


    search = gtk_search_entry_new();
    // gtk_widget_set_halign(search, GTK_ALIGN_CENTER);
    gtk_box_pack_start(GTK_BOX(vbox), search, FALSE, FALSE, 20);



    scrolled_window = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_window), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

    // gtk_container_add(GTK_CONTAINER(window), scrolled_window);
    gtk_box_pack_start(GTK_BOX(vbox), scrolled_window, TRUE, TRUE, 20);




    flow_box = gtk_flow_box_new();
    gtk_flow_box_set_selection_mode(GTK_FLOW_BOX(flow_box), GTK_SELECTION_SINGLE);
    gtk_flow_box_set_homogeneous(GTK_FLOW_BOX(flow_box), TRUE);
    gtk_flow_box_set_row_spacing(GTK_FLOW_BOX(flow_box), BUTTON_ROW_SPACING);
    gtk_flow_box_set_column_spacing(GTK_FLOW_BOX(flow_box), BUTTON_COLUMN_SPACING);
    gtk_flow_box_set_max_children_per_line(GTK_FLOW_BOX(flow_box), MAX_BUTTONS_IN_ROW);
    // gtk_widget_set_vexpand(flow_box, FALSE);
    gtk_widget_set_valign(flow_box, GTK_ALIGN_START);
    gtk_flow_box_set_activate_on_single_click(GTK_FLOW_BOX(flow_box), TRUE);

    // Assign the flow box filter function
    gtk_flow_box_set_filter_func(GTK_FLOW_BOX(flow_box), ab_flowbox_filter_applications, (gpointer) search, NULL);
    // Assign the flow box sort function
    gtk_flow_box_set_sort_func(GTK_FLOW_BOX(flow_box), ab_flowbox_sort_applications, NULL, NULL);
    // Assign the search input changed callback
    g_signal_connect(search, "search-changed", G_CALLBACK(ab_searchentry_changed), (gpointer) flow_box);


    gtk_container_add(GTK_CONTAINER(scrolled_window), flow_box);


    g_signal_connect(window, "destroy", G_CALLBACK(ab_window_destroy), NULL);
    g_signal_connect(window, "key-release-event", G_CALLBACK(ab_window_key_release), (gpointer) flow_box);


    GHashTableIter iter;
    gpointer key, value;

    g_hash_table_iter_init(&iter, apps);

    while (g_hash_table_iter_next(&iter, &key, &value)) {

        GAppInfo *app_info = (GAppInfo*) value;
        GtkWidget *button = ab_button_create(app_info);

        gtk_container_add(GTK_CONTAINER(flow_box), button);
    }


    gtk_widget_show_all(window);

    return 0;
}

/**
 * Read installed applications and stores them in a global variable.
 * Also returns the generated hash table.
 *
 * @return GHashTable<GAppInfo>
 */
GHashTable* ab_get_applications(gboolean show_all)
{
    if (apps != NULL) {
        return apps;
    }

    apps = g_hash_table_new(g_str_hash, g_str_equal);
    GList *node = NULL;
    GAppInfo *app_info = NULL;
    GDesktopAppInfo *desktop_app = NULL;

    node = g_app_info_get_all();

    while (node != NULL) {

        app_info = (GAppInfo*) node->data;
        desktop_app = (GDesktopAppInfo*) app_info;
        // gboolean should_show = g_app_info_should_show(app_info);
        gboolean no_display = g_desktop_app_info_get_nodisplay(desktop_app);
        gboolean show_in = g_desktop_app_info_get_show_in(desktop_app, NULL);
        gboolean is_hidden = g_desktop_app_info_get_is_hidden(desktop_app);

        // if (should_show) {
        if (show_all || (!no_display && show_in && !is_hidden)) {

            // Cache the applications in the global variable app.
            const gchar *app_name = g_app_info_get_display_name(app_info);
            g_hash_table_insert(apps, (gpointer) app_name, (gpointer) app_info);
        }

        node = g_list_next(node);
    }

    return apps;
}

/**
 * Print version.
 *
 * @return void
 */
void ab_show_version()
{
    g_print("%s\n", PACKAGE_STRING);
    g_print("Maintainer:  %s\n", PACKAGE_MAINTAINER);
    g_print("Site:        %s\n", PACKAGE_URL);

    exit(0);
}

/**
 * Print help.
 *
 * @return void
 */
void ab_show_help()
{
    int space = 20;

    g_print("Usage: %s [option]\n\n", PACKAGE);
    g_print("%-*s%s\n", space, " -v --version", ": Print the version and exit.");
    g_print("%-*s%s\n", space, " -h --help", ": Print this message and exit.");
    g_print("%-*s%s\n", space, " -a --all", ": Displays all the applications, even those maked as hidden.");
    g_print("\n");
    g_print("The default behaviour is to display only applications marked as visible.\n");

    exit(0);
}

/**
 * Terminate the program if another instance is currently running.
 *
 * @return void
 */
void ab_ensure_one_process()
{
    pid_t current_pid = getpid();
    pid_t ipid = 0;
    char pidline[1024];
    char *pid = NULL;
    FILE *fp = NULL;

    fp = popen("pidof "PACKAGE, "r");

    fgets(pidline, 1024, fp);
    pid = strtok(pidline, " ");

    while (pid != NULL) {

        ipid = atoi(pid);

        if (ipid != current_pid) {
            pclose(fp);
            g_warning("[INFO]: Aboard already running with pid %d, exiting...", ipid);
            exit(0);
        }

        pid = strtok(NULL, " ");
    }

    pclose(fp);
}

/**
 * Entry point.
 *
 * @param int argc
 * @param char* argv[]
 * @return int
 */
int main(int argc, char* argv[])
{
    gboolean show_all = FALSE;

    if (argv[1] != NULL) {

        gboolean show_version = FALSE;
        gboolean show_help = FALSE;
        show_version = show_version || g_strcmp0(argv[1], "-v") == 0;
        show_version = show_version || g_strcmp0(argv[1], "--version") == 0;
        show_help = show_help || g_strcmp0(argv[1], "-h") == 0;
        show_help = show_help || g_strcmp0(argv[1], "--help") == 0;
        show_all = show_all || g_strcmp0(argv[1], "-a") == 0;
        show_all = show_all || g_strcmp0(argv[1], "--all") == 0;

        if (show_version) {
            ab_show_version();
        } else if (show_help) {
            ab_show_help();
        }
    }

    ab_ensure_one_process();

    gtk_init(&argc, &argv);

    if (ab_set_css_styles() == 0) {

        ab_get_applications(show_all);
        ab_window_init();
        gtk_main();

        // Will execute after gtk_main_quit() is called in ab_window_destroy()

        g_hash_table_destroy(apps);
        g_list_free(visible_apps);
    }

    return 0;
}
