## Aboard, a GTK app launcher

This is a simple application launcher that borrows the concept from that of Gnome. I use it mainly with Openbox and LXQt, and even i3, possibly can be used with others window managers/desktops environment since the only dependency required is GTK, but I haven't tested it.

Applications can be searched by name or categories defined in desktop files.

### Compilation and use

Compilation and installation are pretty straight forward:

    $ ./configure [--help | --prefix=/path]
    $ make
    $ make install

    $ aboard

### Register a key binding

Obviously you want to register a key binding for easy access. For instance, in Openbox you can use `obkey` to do so or just edit `~/.config/openbox/rc.xml` and then add following section:

    <!-- Register Win + A key binding to execute aboard -->
    <keybind key="W-a">
      <action name="Execute">
        <command>aboard</command>
      </action>
    </keybind>
